from django.urls import path, re_path
from . import views

urlpatterns = [
    path('', views.index, name='index'),
    re_path(r'details/(?P<id>[0-9]+)/$', views.details, name='detail'),
    re_path(r'add/$', views.add, name='add'),
]